const fs = require("fs");

module.exports = (req, res, id) => {
    
    if (!fs.existsSync(global.disk + global.uploads + id + "/data.json")) {
        console.log(`No id at ${global.disk}${id}.json`);
        res.send(`No id at ${id}`);
    } else {

        const rawdata = fs.readFileSync(global.disk + global.uploads + id + "/data.json", {encoding:'utf8', flag:'r'});
        const data = JSON.parse(rawdata);
        const canonURL = global.domain + id;
        const posterURL = global.domain + "poster/" + id;
        const videoURL = global.domain + "video/" + id;

        res.writeHead(200, { 'Content-Type': 'application/json' });

        let json = {
            "version": "1.0",
            "title": data.filename,
            "type": "video",
            "height": data.height,
            "width": data.width,
            "provider_name": "nvm.me",
            "provider_url": "https://nvm.me",
            "thumbnail_height": data.height,
            "thumbnail_width": data.width,
            "thumbnail_url": posterURL,
            "html": `<video poster='${posterURL}'><source src='${videoURL}' type='video/mp4' /></video>`
        }

        res.write(JSON.stringify(json));
        return res.end();
    }
}
