const _ = query => document.querySelector(query);

class App {
    constructor() {
        this.defaultTitle = "nvm.me";
        this.appID = "me.nvm.sharedlinks";
        this.domain = "https://nvm.me/";

        this.view = _("#app");
        this.grid = _("#appGrid");

        if (!localStorage.getItem(this.appID)) {
            this.savedLinks = [];
        } else {
            this.savedLinks = this.loadSavedLinks();
        }

        if (this.savedLinks.length > 0) {
            _("#previousLinks").innerHTML += `<h2>Previous links</h2>`;
            Array.from(this.savedLinks).forEach(link => {
                _("#previousLinks").innerHTML += `<a href="${link}">${link}</a>`;
            });
            let logout = document.createElement('button');
            logout.type = 'button';
            logout.innerText = 'Clear all';
            logout.onclick = () => { this.clearAllLinks() };
            _("#previousLinks").append(logout);
        }

        this.ffmpeg = FFmpeg.createFFmpeg({
            log: false,
            corePath: new URL('lib/ffmpeg-core.js', document.location).href,
        });

        _('#uploader').addEventListener('change', () => { this.transcode(event) });
    }
    async transcode({ target: { files } }) {
        const file = files[0];

        if (file.size > 100000000) {
            alert('File is too large! Please select a file less than 100MB.');

        } else {
            _('#landing').remove();
            _('#previousLinks').remove();

            let linkView = document.createElement('section');
            this.grid.append(linkView);

            let linkViewTextContainer = document.createElement('div');
            linkViewTextContainer.className = "textContainer";
            linkViewTextContainer.innerHTML = `<h1>Processing</h1>
            <p>Processing the file  ${file.name}</p>
            <p>Multiple files will be generated and uploaded. Don't close the window, or the process will be incomplete.</p>`;
            linkView.append(linkViewTextContainer);

            let chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
            this.folder = '';
            for (let i = 0; i < 16; i++) {
                this.folder += chars.charAt(Math.floor(Math.random() * chars.length));
            }

            const fn = {
                original: file.name,
                scaled: "video.mp4",
                poster: "poster.jpg"
            }

            // load the giant ffmpeg library
            await this.ffmpeg.load();

            // write the select video in the weird ffmpeg filesystem
            this.ffmpeg.FS('writeFile', fn.original, await FFmpeg.fetchFile(file));

            // Scale down and move the moov atom at the beginning
            // https://sanjeev-pandey.medium.com/understanding-the-mpeg-4-moov-atom-pseudo-streaming-in-mp4-93935e1b9e9a
            let scaled = new Asset(
                this, this.grid,
                this.folder, fn.original, fn.scaled,
                'video/mp4',
                ['-i', fn.original, '-vf', 'scale=\'bitand(oh*dar,65534)\':\'min(1280,ih)\'', '-movflags', 'faststart', '-strict', '-2', '-y', fn.scaled],
                "Optimizing video",
                "<figure>✅</figure>optimized");
            await scaled.run();

            // Extract the first frame as a poster frame
            let poster = new Asset(
                this, this.grid,
                this.folder, fn.original, fn.poster,
                'image/jpg',
                ['-i', fn.original, '-ss', '00:00:00.000', '-vframes', '1', '-y', fn.poster],
                "Extracting poster",
                "<figure>✅</figure>poster");
            await poster.run();

            // Get width and height to use in all the files
            let posterDimentions = poster.getDimensions();
            let width = posterDimentions.width;
            let height = posterDimentions.height;

            let metadata = {
                filename: file.name,
                width: width,
                height: height
            }

            let formData = new FormData();
            var blob = new Blob([JSON.stringify(metadata)], { type: 'application/json' });
            formData.append('file', blob, "data.json");
            formData.append('folder', this.folder);
            await fetch('/store', {
                method: 'POST',
                body: formData
            });

            let link = this.domain + this.folder;

            linkView.className = "success";
            linkViewTextContainer.innerHTML = `<h1>Success</h1>
            <p>Your video has been successfully optimized and uploaded.</p>
            <p>Share this link: <a href="${link}">${link}</a></p>
            <p><a href="/">Share another</a></p>`
            linkView.append(linkViewTextContainer);

            this.saveURL(link);
        }

    }
    clearAllLinks() {
        localStorage.setItem(this.appID, "");
        // window.location = "/";
        location.reload();
    }
    saveURL(url) {
        this.savedLinks.push(url);
        localStorage.setItem(this.appID, JSON.stringify(this.savedLinks));
    }
    loadSavedLinks() {
        return JSON.parse(localStorage.getItem(this.appID));
    }
}

class Asset {
    constructor(app, rootEl, baseFolder, originalFilename, filename, mimetype, ffmpegCmd, label, labelDone) {
        this.app = app;
        this.originalFilename = originalFilename;
        this.filename = filename;
        this.ffmpegCmd = ffmpegCmd;
        this.baseFolder = baseFolder;
        this.mimetype = mimetype;

        this.label = label;
        this.labelDone = labelDone;

        this.view = document.createElement('section');
        rootEl.append(this.view);

        if (mimetype == 'image/jpg') {
            this.mediaEl = document.createElement('img');
        } else {
            this.mediaEl = document.createElement('video');
            this.mediaEl.controls = true;
        }
        this.mediaEl.style.display = "none";
        this.view.append(this.mediaEl);

        this.preEl = document.createElement('pre');
        this.preEl.style.display = "block";
        this.view.append(this.preEl);

        this.progressContainer = document.createElement('div');
        this.progressContainer.className = "progressContainer";
        this.view.append(this.progressContainer);

        this.ffmpeglabel = document.createElement('p');
        this.ffmpeglabel.innerHTML = this.label;
        this.progressContainer.append(this.ffmpeglabel);

        this.encodingProgressEl = document.createElement('progress');
        this.encodingProgressEl.max = 100;
        this.encodingProgressEl.value = 0;
        this.progressContainer.append(this.encodingProgressEl);


    }
    async run() {
        this.app.ffmpeg.setProgress(({ ratio }) => {
            this.encodingProgressEl.value = ratio * 100;
        });

        this.app.ffmpeg.setLogger(({ type, message }) => {
            this.preEl.innerText += message + "\n";
            this.preEl.scrollTop = this.preEl.scrollHeight;
            console.log(type, message);
        });

        await this.app.ffmpeg.run(...this.ffmpegCmd);

        const data = this.app.ffmpeg.FS('readFile', this.filename);
        this.blob = new Blob([data.buffer], { type: this.mimetype });
        this.blobURL = URL.createObjectURL(this.blob);
        this.file = new File([this.blob], this.filename);
        this.mediaEl.src = this.blobURL;
        this.mediaEl.style.display = "block";

        this.ffmpeglabel.innerHTML = this.labelDone;

        this.preEl.remove();

        let formData = new FormData();
        formData.append('file', this.blob, this.filename);
        formData.append('folder', this.baseFolder);
        await fetch('/store', {
            method: 'POST',
            body: formData
        });
    }
    getDimensions() {
        return {
            width: this.mediaEl.width,
            height: this.mediaEl.height
        };
    }
}
