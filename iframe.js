const fs = require("fs");

module.exports = (req, res, id) => {
    
    if (!fs.existsSync(global.disk + global.uploads + id + "/data.json")) {
        console.log(`No id at ${global.disk}${id}.json`);
        res.send(`No id at ${id}`);
    } else {

        const rawdata = fs.readFileSync(global.disk + global.uploads + id + "/data.json", {encoding:'utf8', flag:'r'});
        const data = JSON.parse(rawdata);
        const canonURL = global.domain + id;

        const posterURL = global.domain + "poster/" + id;
        const videoURL = global.domain + "video/" + id;
        const jsonURL = global.domain + "json/" + id;
        const xmlURL = global.domain + "xml/" + id;

        res.writeHead(200, { 'Content-Type': 'text/html' });

        res.write(`<!doctype html>
  <html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
  
      <title>${data.filename}</title>
      <link href="data:image/x-icon;base64,=" rel="icon" type="image/x-icon" />
      <link rel="alternate" type="application/json+oembed" href="${jsonURL}" title="${data.filename}" />
      <link rel="alternate" type="text/xml+oembed" href="${xmlURL}" title="${data.filename}" />
  
      <link rel="canonical" href="${canonURL}">
  
      <meta property="og:site_name" content="nvm.me">
      <meta property="og:url" content="${canonURL}">
      <meta property="og:type" content="video.other">
      <meta property="og:title" content="${data.filename}">
      <meta property="og:description" content="${data.filename}">
      <meta property="og:image" content="${posterURL}">
      <meta property="og:video" content="${videoURL}">
      <meta property="og:video:type" content="video/mp4">
      <meta property="og:video:secure_url" content="${videoURL}">
      <meta property="og:video:height" content="${data.height}">
      <meta property="og:video:width" content="${data.width}">
      <meta property="og:image:height" content="${data.height}">
      <meta property="og:image:width" content="${data.width}">
  
      <meta name="medium" content="video">
      <meta name="description" content="${data.filename}" >
  
      <style type="text/css">
      *, *::before, *::after {
          padding: 0;
          margin: 0;
          box-sizing: border-box;
      }
  
      html, body {
          width: 100%;
          height: 100%;
          background-color: black;
      }
  
      img, picture, video, canvas, svg {
          display: block;
          max-width: 100%;
      }
      </style>
  </head>
  
  <body>
      <video poster='${posterURL}' width="100%" height="100%" style="object-fit: contain;" playsinline autoplay muted loop controls>
      <source src="${videoURL}" type="video/mp4" />
          Your browser does not support HTML5 video.
      </video>
  </body>
  
  </html>`);

        return res.end();
    }
}
