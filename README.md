To run locally:

```bash
LOCAL=true node server.js
```

or 

```bash
LOCAL=true nodemon server.js
```