// const path = require('path')
const express = require('express');
const multer = require('multer');
const fs = require('fs');

const iframe = require('./iframe.js');
const json = require('./json.js');
const xml = require('./xml.js');
// const ls = require('./ls.js');
const poster = require('./poster.js');
const video = require('./video.js');

const app = express();
const publicDir = "/public";

if (process.env.LOCAL) {
    global.domain = "http://localhost:8080/";
    global.disk = "./disk/";
    global.uploads = "uploads/";
    global.isLocal = true;
} else {
    global.domain = "https://nvm.me/";
    global.disk = "/var/data/";
    global.uploads = "uploads/";
    global.isLocal = false;
}

const upload = (folderName) => {
    return imageUpload = multer({
        storage: multer.diskStorage({
            destination: function (req, file, cb) {
                const path = global.disk + folderName;
                fs.mkdirSync(path, { recursive: true })
                cb(null, path);
            },

            filename: function (req, file, cb) {
                cb(null, file.originalname);
            }
        }),
        limits: { fileSize: 200000000 },
        fileFilter: function (req, file, cb) {
            if (!file.originalname.match(/\.(json|mp4|webm|jpg|webp)$/)) {
                req.fileValidationError = 'Only mp4 and jpg files are allowed!';
                return cb(null, false);
            }
            cb(null, true);
        }
    })
}

app.get('/', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With,Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');

    // Those headers are necessary for ffmpeg to run in the browser (SharedArrayBuffer)
    res.header('Cross-Origin-Embedder-Policy', 'require-corp');
    res.header('Cross-Origin-Opener-Policy', 'same-origin');
    res.sendFile(__dirname + "/public/index.html");
})

app.get('/:id([a-zA-Z0-9]{16})', (req, res) => {
    const id = req.params.id;
    iframe(req, res, id);
});

app.get('/json/:id([a-zA-Z0-9]{16})', (req, res) => {
    const id = req.params.id;
    json(req, res, id);
});

app.get('/xml/:id([a-zA-Z0-9]{16})', (req, res) => {
    const id = req.params.id;
    xml(req, res, id);
});

app.get('/poster/:id([a-zA-Z0-9]{16})', (req, res) => {
    const id = req.params.id;
    poster(req, res, id);
});

app.get('/video/:id([a-zA-Z0-9]{16})', (req, res) => {
    const id = req.params.id;
    video(req, res, id);
});

app.post('/store', upload("temp").single('file'), (req, res) => {
    if (req.file) {
        // Get the file and target folder
        const file = req.file;
        const folder = req.body.folder;
        const path = global.disk + "uploads/" + folder;
        fs.mkdirSync(path, { recursive: true })

        // Move the file to the target folder
        fs.rename(file.path, `${path}/${file.filename}`, (err) => {
            if (err) {
                console.error(err);
                return;
            }
            console.log(folder + ': ' + file.filename + ' uploaded successfully');
        });

        res.json({
            file: req.file,
        });
    } else {
        res.status(400).json({
            error: 'Please select a file to upload',
        });
    }
});

app.use(express.static(__dirname + publicDir));

var server = app.listen(process.env.PORT || 8080, function () {
    var host = server.address().address
    var port = server.address().port

    console.log(`Listening at http://localhost:${port}`);
})
