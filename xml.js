const fs = require("fs");

module.exports = (req, res, id) => {

    if (!fs.existsSync(global.disk + global.uploads + id + "/data.json")) {
        console.log(`No id at ${global.disk}${id}.json`);
        res.send(`No id at ${id}`);
    } else {

        const rawdata = fs.readFileSync(global.disk + global.uploads + id + "/data.json", {encoding:'utf8', flag:'r'});
        const data = JSON.parse(rawdata);
        const canonURL = global.domain + id;
        const posterURL = global.domain + "poster/" + id;
        const videoURL = global.domain + "video/" + id;

        res.writeHead(200, { 'Content-Type': 'text/xml' });

        let xml = `<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<oembed>
    <version>1.0</version>
    <title>${data.filename}</title>      
    <type>video</type>
    <type>video</type>
    <width>${data.width}</width>
    <height>${data.height}</height>
    <provider_name>nvm.me</provider_name>
    <provider_url>https://nvm.me/</provider_url>
    <thumbnail_height>${data.height}</thumbnail_height>
    <thumbnail_width>${data.width}</thumbnail_width>
    <thumbnail_url>${posterURL}</thumbnail_url>
    <html>
        <video poster='${posterURL}'><source src='${videoURL}' type='video/mp4' /></video>
    </html>
</oembed>`;

        res.write(xml);
        return res.end();
    }
}

