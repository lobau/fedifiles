const fs = require("fs");

module.exports = (req, res, id) => {
    const filePath = global.disk + global.uploads + id + "/poster.jpg";

    var s = fs.createReadStream(filePath);
    s.on('open', function () {
        res.writeHead(200, { 'Content-Type': 'image/jpeg' });
        s.pipe(res);
    });
    s.on('error', function () {
        var img = `data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==`;
        res.writeHead(200, {
            'Content-Type': 'image/gif',
            'Content-Length': img.length
        });
        res.end(img);
    });
}
